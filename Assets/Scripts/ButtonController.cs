﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("112131");
       if(gameObject.name == "right")
        {

            GameObject.Find("Player").GetComponent<PlayerController>().rb.AddForce(new Vector2(1, 0), ForceMode2D.Impulse);
            
        }
        if (gameObject.name == "left")
        {
            GameObject.Find("Player").GetComponent<PlayerController>().speedX = -1;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        GameObject.Find("Player").GetComponent<PlayerController>().speedX = 0;
    }

    
}
