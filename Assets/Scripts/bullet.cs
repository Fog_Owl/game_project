﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public float speed = 5f;
    public int damage = 1;
    public Rigidbody2D rb;
    
    void Start()
    {
        rb.velocity = -transform.right * speed;
    }
    private void OnTriggerEnter2D(Collider2D hitinfo)
    {
        Debug.Log(hitinfo.name);
        Destroy(gameObject);
        Enemy enemy = hitinfo.GetComponent<Enemy>();
        if(enemy != null)
        {
            enemy.TakeDamage(damage);
        }
        if(hitinfo.gameObject.tag == "Player")
        {
            PlayerController end_game = hitinfo.GetComponent<PlayerController>();
            Debug.Log(end_game);
            if (end_game != null)
            {
                end_game.End_Game();
            }
            
        }
    }

}
