﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Fire : MonoBehaviour
{

    public GameObject[] fire;
    private bool fireon = true;
    

    
    void Start()
    {
        
    }

   
    void Update()
    {
       if(!IsInvoking("Firing"))
        {

            Invoke("Firing", 1);
        }
    }

    void Firing()
    {




        if (fireon == true)
        {
            for (int i = 0; i < fire.Length; i++)
            {
                fire[i].SetActive(false);
                fireon = false;
            }
        }
        else
        {
            for (int i = 0; i < fire.Length; i++)
            {
                fire[i].SetActive(true);
                fireon = true;

            }
        }
        
        

        
    }
}
