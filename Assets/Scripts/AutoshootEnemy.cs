﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoshootEnemy : MonoBehaviour
{

    public GameObject bullet;
    public Transform pointbullet;
   
    void Update()
    {
        if (!IsInvoking("Sec"))
        {
            Invoke("Sec", 5); //выполняем sec каждые 5 секунд
        }
    }

    void Sec()
    {
        Instantiate(bullet, pointbullet.position, pointbullet.rotation); // спавн объекта
    }
}
