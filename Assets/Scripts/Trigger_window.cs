﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_window : MonoBehaviour
{
    public GameObject window;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            window.SetActive(true);
            Time.timeScale = 0;
            
        }
    }
    public void PlayGame() 
    {
        Time.timeScale = 1;
        window.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
