﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_window_controller : MonoBehaviour
{
    public BlackScreen blScreen;
    public GameObject level;
    public GameObject window_open;
    private int trig = 0;
    
    


    private void Update()
    {
        if (blScreen.fadeState == BlackScreen.FadeState.InEnd && trig == 1)
        {
            
            level.SetActive(false);
            window_open.SetActive(true);
            blScreen.fadeState = BlackScreen.FadeState.Out;
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("gear");
            blScreen.fadeState = BlackScreen.FadeState.In;
            trig += 1;
           
        }
    }
   

}
