﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Close_level_controller : MonoBehaviour
{

    
    
    private void Update()
    {
        
    }

    public GameObject end_elevator;
    public Slot dropbattary;
    public GameObject help_window;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(GameObject.FindGameObjectWithTag("buttary") == null)
            {
                dropbattary.Dropitem();
                Vector3 amountToMove = new Vector3(0, -2, 0);
                end_elevator.transform.position = Vector3.Lerp(end_elevator.transform.position, end_elevator.transform.position + amountToMove, 4);
                Debug.Log("end");
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                GameObject.FindGameObjectWithTag("buttary").SetActive(false);
                help_window.SetActive(true);
                Time.timeScale = 0;
            }
            
        }

    }
    public void Play_game()
    {
        Time.timeScale = 1;
        help_window.SetActive(false);
    }
}
