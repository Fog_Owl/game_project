﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    public float horizSpeed ;
    public float speedX = 0;
    public float verticalJump;
    public Rigidbody2D rb;
    bool isGroudeed;
    private bool faceWatch = true;
    Animator anim;
    Animator animcontainer;
    Animator animarmor;
    Animator anim_points;
    public GameObject[] armor;
    public GameObject container;
    public GameObject[] health;
    public GameObject[] points;
    private int i = 0;
    private int stage = 1;
    public GameObject[] stages;
    private Transform positionSpawn;
    public GameObject elevator;
    private int points_controller = 0;
    public GameObject level;
    public GameObject die_window;
    public GameObject end_game;


    // Second level scripts

    public GameObject button_to_press;
    private int index = 0;

    // bullets

    public GameObject firepoint;
    // joystick
    
    private float moveInput;

   

    private void Start()
    {
        
        firepoint.transform.Rotate(0f,180f,0f);
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        animarmor = armor[0].GetComponent<Animator>();


    }
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
       
    }
    public void Leftpress()
    {
        Debug.Log("321");
        speedX = -horizSpeed;
        if (faceWatch == false)
        {
            Flipface();
        }
    }
    public void RightPress()
    {
        speedX = horizSpeed;
        if (faceWatch == true )
        {
            Flipface();
        }
    }
    public void UpPress()
    {
        speedX = 0;
    }
    public void PressJump()
    {
        if(isGroudeed)
        {

            rb.velocity = new Vector2(verticalJump, rb.velocity.y);
            
        }
    }
    

    private void FixedUpdate()
    {
        
        
        rb.velocity = new Vector2(speedX , rb.velocity.x);


        if (Input.GetKey(KeyCode.A))
        {
            speedX = -horizSpeed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            speedX = horizSpeed;
        }
        

        if (Input.GetKeyDown(KeyCode.Space) && isGroudeed)
        {
           
            Debug.Log("xyeta");
            rb.AddForce(new Vector2(0, verticalJump), ForceMode2D.Impulse);
        }
        transform.Translate(speedX, 0, 0);
        

        if(faceWatch == false && Input.GetKey(KeyCode.A))
        {
            Flipface();
        }
        else if(faceWatch == true && Input.GetKey(KeyCode.D))
        {
            Flipface();
        }
        
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "button")
        {
            button_to_press.AddComponent<Rigidbody2D>();
            button_to_press.GetComponent<Rigidbody2D>().mass = 1000;   
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            
        }
        if(collision.gameObject.tag == "close_game")
        {
            End_Game();
           /* */
                
            
        }
        if (collision.gameObject.tag == "point")
        {
            
            collision.gameObject.SetActive(false);
            points_controller += 1;
            Debug.Log(points_controller);

        }
        if (collision.gameObject.tag == "Ground")
        {
            isGroudeed = true;
        }
        if(collision.gameObject.tag == "container")
        {

            anim = GetComponent<Animator>();
            animcontainer = collision.gameObject.GetComponent<Animator>();
            
            
            animcontainer.SetBool("container_open", true);
            anim.SetBool("take_con",true);
            points[index].GetComponent<Animation>().Play("Points");

            if (armor[0] != null)
            {
                animarmor = armor[0].GetComponent<Animator>();
                animarmor.SetBool("armor", true);
            }
            index += 1;


            Debug.Log(index);

        }
        if (collision.gameObject.tag == "elevator_door")
        {
            this.transform.parent = collision.transform;
            Vector3 amountToMove = new Vector3(0, 8, 0);
            
            collision.transform.position = Vector3.Lerp(collision.transform.position, collision.transform.position + amountToMove,  4);
            this.transform.parent = null;
            collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            
        }
        if(collision.gameObject.tag == "end_game_elevator")
        {
            end_game.SetActive(true);
            Debug.Log("asd");
        }
        
        

    }

   

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGroudeed = false;
        }
        if (collision.gameObject.tag == "container")
        {
            animcontainer.SetBool("container_open", false);
            anim.SetBool("take_con", false);
            collision.collider.isTrigger = true;
            if(armor[0] != null)
            {
                animarmor.SetBool("armor", false);
            }
            
        }
        if (collision.gameObject.tag == "elevator_door")
        {
            GameObject ner = collision.gameObject;
            Destroy(ner);
            stage += 1;
            
        }
    }

    void Flipface()
    {
        faceWatch = !faceWatch;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
        firepoint.transform.Rotate(0f,180f,0f);

    }
    public void End_Game()
    {
        try
        {
            Debug.Log(i);
            switch (stage)
            {
                case 1:
                    positionSpawn = stages[0].transform;
                    Vector2 playerpos1 = new Vector2(positionSpawn.transform.position.x, positionSpawn.transform.position.y);
                    this.transform.position = playerpos1;
                    break;
                case 2:
                    positionSpawn = stages[1].transform;
                    Vector2 playerpos2 = new Vector2(positionSpawn.transform.position.x, positionSpawn.transform.position.y);
                    this.transform.position = playerpos2;
                    break;
                case 3:
                    positionSpawn = stages[2].transform;
                    Vector2 playerpos3 = new Vector2(positionSpawn.transform.position.x, positionSpawn.transform.position.y);
                    this.transform.position = playerpos3;
                    break;

            }
            health[i].SetActive(false);

            i += 1;
        }
        catch
        {
            Debug.Log("Смэрть");
            level.SetActive(false);
            die_window.SetActive(true);
        }
    }
}
