﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHold : MonoBehaviour
{
    public bool hold;
    public float distance_hold = 1f;
    RaycastHit2D hit;
    public Transform holdPoint;
    public Transform hitbox;
    void Start()
    {
        
    }

    
    void Update()
    {
       
        if (!hold)
        {
            
            hit = Physics2D.Raycast(hitbox.position, Vector2.one, distance_hold);
            if(hit.collider.tag == "weapon")
            {
               
                hold = true;
            }
        }
        if(hold)
        {
            
            hit.collider.gameObject.transform.position = holdPoint.position;
            if(holdPoint.position.x > transform.position.x && hold == true)
            {
                hit.collider.gameObject.transform.localScale = new Vector2(transform.localScale.x / 1.62f, transform.localScale.y / 1.62f);
            }
            else if (holdPoint.position.x < transform.position.x && hold == true)
            {
                hit.collider.gameObject.transform.localScale = new Vector2(transform.localScale.x / 1.62f, transform.localScale.y / 1.62f);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "weapon")
        {
            Debug.Log("wining"); 
        }
        
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.left * transform.localScale.x * distance_hold);
    }
}
