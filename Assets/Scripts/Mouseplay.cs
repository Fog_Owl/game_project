﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouseplay : MonoBehaviour
{
    public float speed;
    private bool movingRight = true;
    public Transform hitbox;
    public float distance;
    

    private void Start()
    {
        
    }

    private void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        RaycastHit2D hitinfo = Physics2D.Raycast(hitbox.position, Vector2.one, distance);
        if (hitinfo.collider.tag == "Ground")
        {
            if (movingRight == true)
            {
                
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }

    }

    
    
}
