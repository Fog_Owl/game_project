﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForStaticButton : MonoBehaviour
{
    private Main_script inventary;
    public GameObject slotBatary;
    private void Start()
    {
        inventary = GameObject.FindGameObjectWithTag("Player").GetComponent<Main_script>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Player"))
        {
            
            Instantiate(slotBatary, inventary.slotToBatary.transform);
            Destroy(gameObject);
        }
    }
}
