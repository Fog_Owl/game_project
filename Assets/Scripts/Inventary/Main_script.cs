﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_script : MonoBehaviour
{
    public bool[] isFull;
    public GameObject[] slot;
    public GameObject slotToBatary;
    public GameObject inventary;
    private bool inventaryOn;
    private int child;

    private void Start()
    {
        inventaryOn = true;
    }

    public void Chest()
    {
        child = inventary.transform.childCount;

        if(inventaryOn == false)
        {
            for(int i = 0; i < child; i++)
            {
                inventary.transform.GetChild((int)i).gameObject.SetActive(true);
            }
            inventaryOn = true;
            
        }
        else if(inventaryOn == true)
        {
            for (int i = 0; i < child; i++)
            {
                inventary.transform.GetChild((int)i).gameObject.SetActive(false);
            }
            inventaryOn = false;
            
        }
    }
}
