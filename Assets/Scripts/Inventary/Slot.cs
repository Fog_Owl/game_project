﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    public GameObject itemprefab;
    private int countchild;
    public GameObject slot;
    
    
    public void Dropitem()
    {
        countchild = slot.transform.childCount;

        if(countchild > 0)
        {
            itemprefab.GetComponent<Spawn>().SpawnDroppedItem();
            Destroy(transform.GetChild(0).gameObject, 0);
        }
        
            
    }
        
    
}

