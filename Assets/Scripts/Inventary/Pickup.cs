﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private Main_script inventary;
    public GameObject slotButton;
    

    private void Start()
    {
        inventary = GameObject.FindGameObjectWithTag("Player").GetComponent<Main_script>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            for(int i = 0; i < inventary.slot.Length; i++)
            {
                if(inventary.isFull[i] == false)
                {
                    inventary.isFull[i] = true;
                    Instantiate(slotButton, inventary.slot[i].transform);
                    Debug.Log("123");
                    Destroy(gameObject);
                    break;
                }
            }
        }
        
    }
}
