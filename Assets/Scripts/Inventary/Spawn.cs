﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject item;
    public Transform positionPlayer;
    private void Start()
    {
        
    }

    public void SpawnDroppedItem()
    {
        positionPlayer = GameObject.FindGameObjectWithTag("eyes_player").transform;
        Vector2 playerPos = new Vector2(positionPlayer.transform.position.x  , positionPlayer.transform.position.y );
        Instantiate(item, playerPos,Quaternion.identity);
    }
}
