﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_manager : MonoBehaviour
{
    public Animator btn_start;
    public Animator btn_settings;
    public Animator btn_exit;
    public Animator window_settings;
    public Animator pnl_content;
    public Animator gearImg;
    public GameObject[] window;
    public GameObject level;
    public BlackScreen blScreen;
    private int i = 0;
    public GameObject window_about;
    int sceneIndex;
    public GameObject mob;
    public GameObject interfaceshoot;
    private void Start()
    {
        RectTransform transform = pnl_content.gameObject.transform as RectTransform;
        Vector2 position = transform.anchoredPosition;
        position.y -= transform.rect.height;
        transform.anchoredPosition = position;
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void Btn_Close()
    {
        btn_settings.SetBool("isHideSettings",true);
        btn_exit.SetBool("isHide_exit", true);
        btn_start.SetBool("isHide",true);
        window_settings.SetBool("isHide",false);
    }
    public void Btn_Open()
    {
        btn_settings.SetBool("isHideSettings", false);
        btn_exit.SetBool("isHide_exit", false);
        btn_start.SetBool("isHide", false);
        window_settings.SetBool("isHide",true);
    }
    public void Btn_Exit()
    {
        Debug.Log("exit");
        Application.Quit();
    }
    public void ToggleMenu()
    {

        pnl_content.enabled = true;
        bool isHide = pnl_content.GetBool("IsHide");
        pnl_content.SetBool("IsHide",!isHide);
        gearImg.enabled = true;
        gearImg.SetBool("IsHide", !isHide);

    }
    private void FixedUpdate()
    {
        if (blScreen.fadeState == BlackScreen.FadeState.InEnd)
        {
            window[i].SetActive(false);
            level.SetActive(true);
            blScreen.fadeState = BlackScreen.FadeState.Out;
            i = i + 1;
            if(i == 1)
            {
                interfaceshoot.SetActive(true);
            }
            if(i == 2)
            {
                mob.SetActive(true);

            }

            Debug.Log(i);

        }
    }
    public void Close_Window()
    {
        blScreen.fadeState = BlackScreen.FadeState.In;
        
        

    }
   
    public void Load_Menu()
    {
        SceneManager.LoadScene(0);
    }
    public void Load_NextLevel()
    {
        if(sceneIndex<2)
        {
            SceneManager.LoadScene(sceneIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
        
    }
    public void Load_Level()
    {
        SceneManager.LoadScene(sceneIndex);
    }
    public void Load_about()
    {
        Time.timeScale = 0;
        window_about.SetActive(true);
    }
    public void Close_about()
    {
        Time.timeScale = 1;
        window_about.SetActive(false);
    }
}
