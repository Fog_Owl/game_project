﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject points;
    public int health = 2;
    public Transform miss;
    
    
    public void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "miss")
        {

            Debug.Log("werty");
            Instantiate(points,miss.position,miss.rotation);
            Destroy(this.gameObject);
        }
    }
}
